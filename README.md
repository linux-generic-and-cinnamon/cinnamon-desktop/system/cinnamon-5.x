**ExtensionCore.py**

This file is located at `/usr/share/cinnamon/cinnamon-settings/bin/`

Changes consist in displaying minimum Cinnamon version for each multiversion Spice, as well as displaying enabled Spices by user/system, and total installed/enabled.

This should spot incompatible versions as well as possibly too old Spices that may need updating.

***
**panel.js**

This file is located at `/usr/share/cinnamon/js/ui/` and should always be paired with **cs_panel.py**.

These both push the limits of panel height/width considering an autohidden panel can have any size up to screen height/width. There still are limits but hopefully they are more acceptable than the official defaults.

Using the **Quicklaunch** panel-launchers applet would allow multi-line launchers so a high/wide panel would provide more space for window buttons in Traditional mode. Combined with the **Multiline Window List** applet this would offer a nice legacy environment.
***
**cs_panel.py**

This file is located at `/usr/share/cinnamon/cinnamon-settings/modules/` and should always be paired with **panel.js**. See above.